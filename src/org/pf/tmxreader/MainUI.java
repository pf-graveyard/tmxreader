/*

 Copyright (C) 2011 Oleksandr Natalenko aka post-factum

 This program is free software; you can redistribute it and/or modify
 it under the terms of the Universal Program License as published by
 Oleksandr Natalenko aka post-factum; see file COPYING for details.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 You should have received a copy of the Universal Program
 License along with this program; if not, write to
 pfactum@gmail.com

*/

package org.pf.tmxreader;

import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.*;
import java.io.File;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;

import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;

public class MainUI extends JPanel {
	TMXReaderController readerController;
	Thread readerControllerThread;
	JFrame frame;

	public MainUI(JFrame _frame) {
		initComponents();
		frame = _frame;
	}

	private void doSearch(TMXReaderController.Direction _direction)
	{
		if (!searchPhraseField.getText().isEmpty() && readerController != null)
		{
			readerController.setText(searchPhraseField.getText());
			readerController.setDirection(_direction);
			try
			{
				//destroys controller thread before new search
				//If not, exception may occure
				if (readerControllerThread != null && readerControllerThread.isAlive())
				{
					readerControllerThread.interrupt();
					readerControllerThread.join();
				}
			} catch (Exception ex)
			{
				ex.getLocalizedMessage();
			}
			readerControllerThread = new Thread(readerController);
			readerControllerThread.start();
		}
	}

	private String getClipboardContents()
	{
		String result = "";
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		Transferable contents = clipboard.getContents(null);
		boolean hasTransferableText = (contents != null) && contents.isDataFlavorSupported(DataFlavor.stringFlavor);
		if (hasTransferableText)
		{
			try
			{
				result = (String)contents.getTransferData(DataFlavor.stringFlavor);
			}
			catch (Exception ex)
			{
				System.out.println(ex.getLocalizedMessage());
			}
		}
		return result;
	}

	private void sourceTextFieldKeyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER && !e.isControlDown())
			doSearch(TMXReaderController.Direction.FORWARD);
		else if (e.getKeyCode() == KeyEvent.VK_ENTER && e.isControlDown())
			doSearch(TMXReaderController.Direction.BACKWARD);
	}

	private void exitItemActionPerformed(ActionEvent e) {
		System.exit(0);
	}

	private void openFileItemActionPerformed(ActionEvent e) {
		JFileChooser chooser = new JFileChooser();
		chooser.setApproveButtonText("Open");
		chooser.setDialogTitle("Open exported translation memory");
		chooser.setFileFilter(new FileFilter()
		{
			@Override
			public boolean accept(File file)
			{
				return file.isDirectory() || file.getName().endsWith(".tmx") || file.getName().endsWith(".TMX");
			}

			@Override
			public String getDescription()
			{
				return "Exported translation memory (*.tmx, *.TMX)";
			}
		});
		int result = chooser.showOpenDialog(this);
		if (result != JFileChooser.CANCEL_OPTION)
		{
			File file = chooser.getSelectedFile();
			readerController = new TMXReaderController(file, resultsPane, statusLabel, sourceLanguageField, targetLanguageField, resultsLabel);
			fileLabel.setText("File: " + file.getName());
			frame.setTitle("TMXReader - " + file.getName());
		}
	}

	private void aboutItemActionPerformed(ActionEvent e) {
		aboutDialog.setVisible(true);
	}

	private void findButtonActionPerformed(ActionEvent e) {
		if ((e.getModifiers() & ActionEvent.CTRL_MASK) == ActionEvent.CTRL_MASK)
			doSearch(TMXReaderController.Direction.BACKWARD);
		else
			doSearch(TMXReaderController.Direction.FORWARD);
		searchPhraseField.requestFocus();
	}

	private void OKButtonActionPerformed(ActionEvent e) {
		if (sourceLanguageField.getText().isEmpty())
		{
			sourceLanguageField.requestFocus();
			return;
		}
		if (targetLanguageField.getText().isEmpty())
		{
			targetLanguageField.requestFocus();
			return;
		}
		if (readerController == null)
		{
			languageSettingsDialog.setVisible(false);
			return;
		}

		readerController.setSourceLanguage(sourceLanguageField.getText());
		readerController.setTargetLanguage(targetLanguageField.getText());
		resultsLabel.setText("Language pair: " + readerController.getSourceLanguage() + ", " + readerController.getTargetLanguage());
		languageSettingsDialog.setVisible(false);
	}

	private void changeLanguagesItemActionPerformed(ActionEvent e) {
		languageSettingsDialog.setVisible(true);
	}

	private void menuItem1ActionPerformed(ActionEvent e) {
		shortcutsDialog.setVisible(true);
	}

	private void shortcutsDialogKeyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
			shortcutsDialog.setVisible(false);
	}

	private void showLicenseButtonActionPerformed(ActionEvent e) {
		licenseDialog.setLocation(0, 0);
		licenseDialog.setVisible(true);
		licenseDialog.setSize(600, 500);
	}

	private void clipboardLookForwardItemActionPerformed(ActionEvent e) {
		searchPhraseField.setText(getClipboardContents());
		doSearch(TMXReaderController.Direction.FORWARD);
		searchPhraseField.requestFocus();
	}

	private void clipboardLookBackwardItemActionPerformed(ActionEvent e) {
		searchPhraseField.setText(getClipboardContents());
		doSearch(TMXReaderController.Direction.BACKWARD);
		searchPhraseField.requestFocus();
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        menuBar = new JMenuBar();
        fileMenu = new JMenu();
        openFileItem = new JMenuItem();
        exitItem = new JMenuItem();
        translationMenu = new JMenu();
        clipboardLookForwardItem = new JMenuItem();
        clipboardLookBackwardItem = new JMenuItem();
        settingsMenu = new JMenu();
        changeLanguagesItem = new JMenuItem();
        helpMenu = new JMenu();
        menuItem1 = new JMenuItem();
        aboutItem = new JMenuItem();
        sourceStringLabel = new JLabel();
        searchPhraseField = new JTextField();
        findButton = new JButton();
        resultsLabel = new JLabel();
        scrollPane1 = new JScrollPane();
        resultsPane = new JEditorPane();
        statusPanel = new JPanel();
        fileLabel = new JLabel();
        statusLabel = new JLabel();
        aboutDialog = new JDialog();
        descriptionLabel = new JLabel();
        authorLabel = new JLabel();
        emailLabel = new JLabel();
        websiteLabel = new JLabel();
        janusLabel = new JLabel();
        distributionLabel = new JLabel();
        UPLLabel = new JLabel();
        showLicenseButton = new JButton();
        languageSettingsDialog = new JDialog();
        sourceLanguageLabel = new JLabel();
        sourceLanguageField = new JTextField();
        targetLanguageLabel = new JLabel();
        targetLanguageField = new JTextField();
        OKButton = new JButton();
        shortcutsDialog = new JDialog();
        label1 = new JLabel();
        label5 = new JLabel();
        label2 = new JLabel();
        label6 = new JLabel();
        label3 = new JLabel();
        label7 = new JLabel();
        label17 = new JLabel();
        label18 = new JLabel();
        label4 = new JLabel();
        label8 = new JLabel();
        label9 = new JLabel();
        label12 = new JLabel();
        label13 = new JLabel();
        label14 = new JLabel();
        label15 = new JLabel();
        label16 = new JLabel();
        label10 = new JLabel();
        label11 = new JLabel();
        licenseDialog = new JDialog();
        scrollPane2 = new JScrollPane();
        textArea1 = new JTextArea();

        //======== this ========
        setLayout(new FormLayout(
            "left:69dlu, $lcgap, 51dlu, $lcgap, default, $lcgap, left:default:grow, $lcgap, default",
            "3*(default, $lgap), fill:83dlu:grow, $lgap, default"));

        //======== menuBar ========
        {

            //======== fileMenu ========
            {
                fileMenu.setText("File");

                //---- openFileItem ----
                openFileItem.setText("Open\u2026");
                openFileItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_MASK));
                openFileItem.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        openFileItemActionPerformed(e);
                    }
                });
                fileMenu.add(openFileItem);

                //---- exitItem ----
                exitItem.setText("Quit");
                exitItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, KeyEvent.CTRL_MASK));
                exitItem.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        exitItemActionPerformed(e);
                    }
                });
                fileMenu.add(exitItem);
            }
            menuBar.add(fileMenu);

            //======== translationMenu ========
            {
                translationMenu.setText("Translation");

                //---- clipboardLookForwardItem ----
                clipboardLookForwardItem.setText("Paste and look forward");
                clipboardLookForwardItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, KeyEvent.CTRL_MASK));
                clipboardLookForwardItem.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        clipboardLookForwardItemActionPerformed(e);
                    }
                });
                translationMenu.add(clipboardLookForwardItem);

                //---- clipboardLookBackwardItem ----
                clipboardLookBackwardItem.setText("Paste and look backward");
                clipboardLookBackwardItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, KeyEvent.CTRL_MASK));
                clipboardLookBackwardItem.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        clipboardLookBackwardItemActionPerformed(e);
                    }
                });
                translationMenu.add(clipboardLookBackwardItem);
            }
            menuBar.add(translationMenu);

            //======== settingsMenu ========
            {
                settingsMenu.setText("Options");

                //---- changeLanguagesItem ----
                changeLanguagesItem.setText("Change languages\u2026");
                changeLanguagesItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0));
                changeLanguagesItem.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        changeLanguagesItemActionPerformed(e);
                    }
                });
                settingsMenu.add(changeLanguagesItem);
            }
            menuBar.add(settingsMenu);

            //======== helpMenu ========
            {
                helpMenu.setText("Help");

                //---- menuItem1 ----
                menuItem1.setText("Keyboard shortcuts\u2026");
                menuItem1.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
                menuItem1.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        menuItem1ActionPerformed(e);
                    }
                });
                helpMenu.add(menuItem1);

                //---- aboutItem ----
                aboutItem.setText("About\u2026");
                aboutItem.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        aboutItemActionPerformed(e);
                    }
                });
                helpMenu.add(aboutItem);
            }
            menuBar.add(helpMenu);
        }
        add(menuBar, CC.xywh(1, 1, 9, 1));

        //---- sourceStringLabel ----
        sourceStringLabel.setText("Search phrase:");
        add(sourceStringLabel, CC.xy(1, 3));

        //---- searchPhraseField ----
        searchPhraseField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                sourceTextFieldKeyPressed(e);
            }
        });
        add(searchPhraseField, CC.xywh(3, 3, 5, 1, CC.FILL, CC.DEFAULT));

        //---- findButton ----
        findButton.setText("Find");
        findButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                findButtonActionPerformed(e);
                findButtonActionPerformed(e);
            }
        });
        add(findButton, CC.xy(9, 3));

        //---- resultsLabel ----
        resultsLabel.setText("Language pair is not detected");
        add(resultsLabel, CC.xywh(1, 5, 9, 1));

        //======== scrollPane1 ========
        {

            //---- resultsPane ----
            resultsPane.setContentType("text/html");
            resultsPane.setEditable(false);
            scrollPane1.setViewportView(resultsPane);
        }
        add(scrollPane1, CC.xywh(1, 7, 9, 1, CC.FILL, CC.FILL));

        //======== statusPanel ========
        {
            statusPanel.setLayout(new FormLayout(
                "default, right:default:grow",
                "default"));

            //---- fileLabel ----
            fileLabel.setText("File is not opened");
            statusPanel.add(fileLabel, CC.xy(1, 1));

            //---- statusLabel ----
            statusLabel.setText("Ready");
            statusPanel.add(statusLabel, CC.xy(2, 1));
        }
        add(statusPanel, CC.xywh(1, 9, 9, 1));

        //======== aboutDialog ========
        {
            aboutDialog.setResizable(false);
            aboutDialog.setTitle("About TMXReader");
            Container aboutDialogContentPane = aboutDialog.getContentPane();
            aboutDialogContentPane.setLayout(new FormLayout(
                "default",
                "10*(default, $lgap), default"));

            //---- descriptionLabel ----
            descriptionLabel.setText("TMXReader v0.0.3 \u2014 *.tmx files viewer");
            aboutDialogContentPane.add(descriptionLabel, CC.xy(1, 1));

            //---- authorLabel ----
            authorLabel.setText("Author and developer: Oleksandr Natalenko");
            aboutDialogContentPane.add(authorLabel, CC.xy(1, 5));

            //---- emailLabel ----
            emailLabel.setText("E-mail: oleksandr@natalenko.name");
            aboutDialogContentPane.add(emailLabel, CC.xy(1, 7));

            //---- websiteLabel ----
            websiteLabel.setText("Web-site: http://natalenko.name/");
            aboutDialogContentPane.add(websiteLabel, CC.xy(1, 9));

            //---- janusLabel ----
            janusLabel.setText("Sponsored by Janus: http://januswwi.com/");
            aboutDialogContentPane.add(janusLabel, CC.xy(1, 13));

            //---- distributionLabel ----
            distributionLabel.setText("Distributed under terms and conditions");
            aboutDialogContentPane.add(distributionLabel, CC.xy(1, 17));

            //---- UPLLabel ----
            UPLLabel.setText("of Universal Program License v4.2");
            aboutDialogContentPane.add(UPLLabel, CC.xy(1, 19));

            //---- showLicenseButton ----
            showLicenseButton.setText("Show License");
            showLicenseButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    showLicenseButtonActionPerformed(e);
                }
            });
            aboutDialogContentPane.add(showLicenseButton, CC.xy(1, 21));
            aboutDialog.pack();
            aboutDialog.setLocationRelativeTo(aboutDialog.getOwner());
        }

        //======== languageSettingsDialog ========
        {
            languageSettingsDialog.setTitle("Change languages");
            languageSettingsDialog.setResizable(false);
            Container languageSettingsDialogContentPane = languageSettingsDialog.getContentPane();
            languageSettingsDialogContentPane.setLayout(new FormLayout(
                "default, $lcgap, 40dlu",
                "2*(default, $lgap), default"));

            //---- sourceLanguageLabel ----
            sourceLanguageLabel.setText("Source language:");
            languageSettingsDialogContentPane.add(sourceLanguageLabel, CC.xy(1, 1));
            languageSettingsDialogContentPane.add(sourceLanguageField, CC.xy(3, 1));

            //---- targetLanguageLabel ----
            targetLanguageLabel.setText("Target language:");
            languageSettingsDialogContentPane.add(targetLanguageLabel, CC.xy(1, 3));
            languageSettingsDialogContentPane.add(targetLanguageField, CC.xy(3, 3));

            //---- OKButton ----
            OKButton.setText("OK");
            OKButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    OKButtonActionPerformed(e);
                }
            });
            languageSettingsDialogContentPane.add(OKButton, CC.xy(3, 5));
            languageSettingsDialog.pack();
            languageSettingsDialog.setLocationRelativeTo(languageSettingsDialog.getOwner());
        }

        //======== shortcutsDialog ========
        {
            shortcutsDialog.setTitle("Keyboard shurtcuts");
            shortcutsDialog.setResizable(false);
            shortcutsDialog.addKeyListener(new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent e) {
                    shortcutsDialogKeyPressed(e);
                }
            });
            Container shortcutsDialogContentPane = shortcutsDialog.getContentPane();
            shortcutsDialogContentPane.setLayout(new FormLayout(
                "default, $lcgap, default",
                "9*(default, $lgap), default"));

            //---- label1 ----
            label1.setText("Open file:");
            shortcutsDialogContentPane.add(label1, CC.xy(1, 1));

            //---- label5 ----
            label5.setText("Ctrl+O");
            shortcutsDialogContentPane.add(label5, CC.xy(3, 1));

            //---- label2 ----
            label2.setText("Close application:");
            shortcutsDialogContentPane.add(label2, CC.xy(1, 3));

            //---- label6 ----
            label6.setText("Ctrl+Q");
            shortcutsDialogContentPane.add(label6, CC.xy(3, 3));

            //---- label3 ----
            label3.setText("Change language pair:");
            shortcutsDialogContentPane.add(label3, CC.xy(1, 5));

            //---- label7 ----
            label7.setText("F2");
            shortcutsDialogContentPane.add(label7, CC.xy(3, 5));

            //---- label17 ----
            label17.setText("Search:");
            shortcutsDialogContentPane.add(label17, CC.xy(1, 7));

            //---- label18 ----
            label18.setText("Enter");
            shortcutsDialogContentPane.add(label18, CC.xy(3, 7));

            //---- label4 ----
            label4.setText("Inverted search:");
            shortcutsDialogContentPane.add(label4, CC.xy(1, 9));

            //---- label8 ----
            label8.setText("Ctrl+Enter in search phrase field");
            shortcutsDialogContentPane.add(label8, CC.xy(3, 9));

            //---- label9 ----
            label9.setText("or Ctrl-click Find button");
            shortcutsDialogContentPane.add(label9, CC.xy(3, 11));

            //---- label12 ----
            label12.setText("Paste and search:");
            shortcutsDialogContentPane.add(label12, CC.xy(1, 13));

            //---- label13 ----
            label13.setText("Ctrl+F");
            shortcutsDialogContentPane.add(label13, CC.xy(3, 13));

            //---- label14 ----
            label14.setText("Paste and inverted");
            shortcutsDialogContentPane.add(label14, CC.xy(1, 15));

            //---- label15 ----
            label15.setText("search:");
            shortcutsDialogContentPane.add(label15, CC.xy(1, 17));

            //---- label16 ----
            label16.setText("Ctrl+G");
            shortcutsDialogContentPane.add(label16, CC.xy(3, 17));

            //---- label10 ----
            label10.setText("This help:");
            shortcutsDialogContentPane.add(label10, CC.xy(1, 19));

            //---- label11 ----
            label11.setText("F1");
            shortcutsDialogContentPane.add(label11, CC.xy(3, 19));
            shortcutsDialog.pack();
            shortcutsDialog.setLocationRelativeTo(shortcutsDialog.getOwner());
        }

        //======== licenseDialog ========
        {
            licenseDialog.setTitle("License");
            licenseDialog.setMinimumSize(new Dimension(10, 30));
            Container licenseDialogContentPane = licenseDialog.getContentPane();
            licenseDialogContentPane.setLayout(new FormLayout(
                "default:grow",
                "fill:default:grow"));

            //======== scrollPane2 ========
            {
                scrollPane2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
                scrollPane2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

                //---- textArea1 ----
                textArea1.setEditable(false);
                textArea1.setText("TMXReader \u2014 *.tmx files reader\n-----------------------------------------------\n\t\tUniversal Program License\n\t\tversion 4.2, (C) Oleksandr Natalenko, 2004-2012\n\n\n\t\tThe legitimate author of this text is Oleksandr Natalenko.\n\n\n\t\tDefinitions\n\n\n\t\"The License\" refers to the text of Universal Program License, which version is pointed in the header of this document.\n\n\t\"The program\" refers to the binary code, that is compiled or translated into executable machine code from the original source code in any appropriate way.\n\n\t\"Free program\" refers to the program, that may be used, modified, distributed and received according to the terms and conditions of the License.\n\n\t\"The copy of the program\" refers to the identical to original stand-alone copy of the program.\n\n\t\"Source code\" refers to the human-readable form of the program that fits the best to modify the program.\n\n\t\"Binary code\" refers to the form of the program, that fits the best to be executed on an appropriate equipment.\n\n\t\"To distribute\" means to transmit the copy of the program, its source code, the copy of the License, all rights and duties, that are defined in the License, simultaneously under the terms and conditions of the License. This definition also includes a desire to transmit the copy of the program and its source code expressed by giving a possibility to get the copy of the program and its source code in a free, free-of-charge and public way (e.g., from a web-site).\n\n\t\"To modify\" means to change the program, its copy and/or source code according to the terms and conditions of the License.\n\n\t\"To use\" means a possibility to execute the program or its copies on an appropriate equipment according to the terms and conditions of the License.\n\n\t\"To receive\" means to get the copy of the program, its source code, the copy of the License, all rights and duties simultaneously by receiver according to the terms and conditions of the License.\n\n\tFirst mentioning about a program, that is distributed under terms and conditions of the License, in each article looks like \"the program, licensed under the License\". Other mentions about a program in the same article look like \"the program\".\n\n\tEach licensee is addressed as \"you\".\n\n\n\t\tBasics\n\n\n\tEveryone is allowed to distribute the License freely, but only the author of the License has a right of modifying it.\n\n\tThe License is created to provide a freedom of using, modifying, distributing and receiving free and free-of-charge programs, their copies and source code.\n\n\tThe License can be translated into any language in condition of keeping the exact contents of the License. The translations of the License are not official. English is the official language of the License.\n\n\tEach new version of the License is numbered in an increasing order. All previous versions are still applicable to your programs.\n\n\tBy applying the License to your program you agree with the terms and conditions of the License and undertake to follow them.\n\n\n\t\tTerms and conditions\n\n\n0. The License can be applied to any program, if its further existing does not conflict with the terms and conditions of the License. No special allowance of the License's author is required.\n\n\n1. You may use the program, licensed under the License, or its copies for any purpose except:\n\na) making any intended damage;\n\nb) acting out of law;\n\nc) committing military aggression.\n\n\n2. You may modify the program, licensed under the License, its copies and source code by following the next conditions:\n\na) you must respect the terms and conditions of the licenses, that are applied to other programs and/or their source code if they or their parts are included into the program;\n\nb) you must include warnings about changes and the list of them;\n\nc) the result of modifying must be licensed under the License.\n\n\n3. You may distribute copies of the program, licensed under the License, by following the next conditions:\n\na) the copy of the program must be distributed with full source code to all its parts, including execution and/or installation scripts if they exist. It is not obligatory to distribute base components (e.g. compiler, operating system, third-party libraries) with the program. If the copy of the program is not provided with its source code, it must be accessible publicly, freely and free-of-charge from shared places (e.g. from web-site);\n\nb) the copy of the program must be provided with electronic or printed copy of the License, that has infinite uninterrupted time of act on the copy of the program and its source code;\n\nc) by distributing the copy of the program you cannot transmit to the user less rights than is defined by the License.\n\n\n4. You may receive the copy of the program, licensed under the License and its source code freely and free-of-charge.\n\n\tGratuitousness of receiving the program's copy and its source code is not dispersed on physical data storage, data writing attendance or receiving it via fee-paying ways (e.g. post or Internet).\n\n\n5. The License prohibits getting the patent on the algorithms, that are used in the program, licensed under the License, its copies and source code.\n\n\n6. There is no warranty for the program, licensed under the License. The entire risk as to the quality and performance of the program is with you. Should the program prove defective, you assume the cost of all necessary servicing, repair or correction.\n\n\n\t\tAppendixes\n\n\n\tAppendix A:  How to apply the License to your programs.\n\n\tTo apply the License to your programs you have to add the following lines at the beginning of this document:\n\n\t<The name of the program>\n\tCopyright (C) <Author>, 20xx\n\t<Additional information about the program and its author>\n\n\n\tAppendix B: How to get the License.\n\n\tThe newest version of the License is publicly available via following URL: http://natalenko.name/myfiles/upl.txt\n\n\n\tAppendix C: The recommendation of using the License.\n\t\n\tIt is recommended to use the last version of the License as it is constantly developed by its author.\n\n\n\tAppendix D: How to contribute.\n\n\tIf you are interested in developing of this License or have some ideas to improve it, please, write to the author of the License: Oleksandr Natalenko <pfactum@gmail.com>.");
                textArea1.setLineWrap(true);
                textArea1.setWrapStyleWord(true);
                textArea1.setFont(new Font("Dialog", Font.PLAIN, 16));
                scrollPane2.setViewportView(textArea1);
            }
            licenseDialogContentPane.add(scrollPane2, CC.xy(1, 1));
            licenseDialog.pack();
            licenseDialog.setLocationRelativeTo(licenseDialog.getOwner());
        }
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JMenuBar menuBar;
    private JMenu fileMenu;
    private JMenuItem openFileItem;
    private JMenuItem exitItem;
    private JMenu translationMenu;
    private JMenuItem clipboardLookForwardItem;
    private JMenuItem clipboardLookBackwardItem;
    private JMenu settingsMenu;
    private JMenuItem changeLanguagesItem;
    private JMenu helpMenu;
    private JMenuItem menuItem1;
    private JMenuItem aboutItem;
    private JLabel sourceStringLabel;
    private JTextField searchPhraseField;
    private JButton findButton;
    private JLabel resultsLabel;
    private JScrollPane scrollPane1;
    private JEditorPane resultsPane;
    private JPanel statusPanel;
    private JLabel fileLabel;
    private JLabel statusLabel;
    private JDialog aboutDialog;
    private JLabel descriptionLabel;
    private JLabel authorLabel;
    private JLabel emailLabel;
    private JLabel websiteLabel;
    private JLabel janusLabel;
    private JLabel distributionLabel;
    private JLabel UPLLabel;
    private JButton showLicenseButton;
    private JDialog languageSettingsDialog;
    private JLabel sourceLanguageLabel;
    private JTextField sourceLanguageField;
    private JLabel targetLanguageLabel;
    private JTextField targetLanguageField;
    private JButton OKButton;
    private JDialog shortcutsDialog;
    private JLabel label1;
    private JLabel label5;
    private JLabel label2;
    private JLabel label6;
    private JLabel label3;
    private JLabel label7;
    private JLabel label17;
    private JLabel label18;
    private JLabel label4;
    private JLabel label8;
    private JLabel label9;
    private JLabel label12;
    private JLabel label13;
    private JLabel label14;
    private JLabel label15;
    private JLabel label16;
    private JLabel label10;
    private JLabel label11;
    private JDialog licenseDialog;
    private JScrollPane scrollPane2;
    private JTextArea textArea1;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
