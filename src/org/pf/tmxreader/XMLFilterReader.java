/*

 Copyright (C) 2011 Oleksandr Natalenko aka post-factum

 This program is free software; you can redistribute it and/or modify
 it under the terms of the Universal Program License as published by
 Oleksandr Natalenko aka post-factum; see file COPYING for details.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 You should have received a copy of the Universal Program
 License along with this program; if not, write to
 pfactum@gmail.com

*/

package org.pf.tmxreader;

import java.io.FilterReader;
import java.io.IOException;
import java.io.Reader;

public class XMLFilterReader extends FilterReader {
	public XMLFilterReader(Reader in) {
		super(in);
	}

	@Override
	public int read() throws IOException
	{
		int read = super.read();

		if (read == -1)
			return -1;

		if ((read == 0x9) ||
			(read == 0xA) ||
			(read == 0xD) ||
			((read >= 0x20) && (read <= 0xD7FF)) ||
			((read >= 0xE000) && (read <= 0xFFFD)) ||
			((read >= 0x10000) && (read <= 0x10FFFF)))
			return read;
		else
			return 0xD;
	}

	@Override
	public int read(char[] _charBuffer, int _offset, int _length) throws IOException
	{
		int read = super.read(_charBuffer, _offset, _length);

		if (read == -1)
			return -1;

		int position = _offset - 1;

		for (int readPosition = _offset; readPosition < _offset + read; readPosition++)
		{
			if ((_charBuffer[readPosition] == 0x9) ||
                (_charBuffer[readPosition] == 0xA) ||
                (_charBuffer[readPosition] == 0xD) ||
                ((_charBuffer[readPosition] >= 0x20) && (_charBuffer[readPosition] <= 0xD7FF)) ||
                ((_charBuffer[readPosition] >= 0xE000) && (_charBuffer[readPosition] <= 0xFFFD)) ||
                ((_charBuffer[readPosition] >= 0x10000) && (_charBuffer[readPosition] <= 0x10FFFF)))
				position++;
			else
				continue;

			if (position < readPosition)
				_charBuffer[position] = _charBuffer[readPosition];
		}

		return position - _offset + 1;
	}

}
