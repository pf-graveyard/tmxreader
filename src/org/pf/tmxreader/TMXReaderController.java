/*

 Copyright (C) 2011 Oleksandr Natalenko aka post-factum

 This program is free software; you can redistribute it and/or modify
 it under the terms of the Universal Program License as published by
 Oleksandr Natalenko aka post-factum; see file COPYING for details.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 You should have received a copy of the Universal Program
 License along with this program; if not, write to
 pfactum@gmail.com

*/

package org.pf.tmxreader;

import javax.swing.*;
import java.io.File;
import java.util.ArrayList;

public class TMXReaderController implements Runnable
{
	private TMXReader reader;
	private JEditorPane editorPane;
	private JLabel statusLabel;
	private String searchPhrase;
	public enum Direction {
		FORWARD,
		BACKWARD
	}

	private Direction direction = Direction.FORWARD;

	public TMXReaderController(File _file, JEditorPane _editorPane, JLabel _statusLabel, JTextField _sourceLanguageField, JTextField _targetLanguageField, JLabel _resultsLabel)
	{
		editorPane = _editorPane;
		statusLabel = _statusLabel;
		reader = new TMXReader(_file);
		_sourceLanguageField.setText(reader.getSourceLanguage());
		_targetLanguageField.setText(reader.getTargetLanguage());
		_resultsLabel.setText("Language pair: " + reader.getSourceLanguage() + ", " + reader.getTargetLanguage());
	}

	public void setText(String _searchPhrase)
	{
		searchPhrase = _searchPhrase;
	}

	public void setDirection(Direction _direction)
	{
		direction = _direction;
	}

	@Override
	public void run()
	{
		statusLabel.setText("Searching matches…");
		ArrayList<String> translations;
		switch (direction)
		{
			case FORWARD:
				translations = reader.getTranslation(searchPhrase);
				break;
			case BACKWARD:
				translations = reader.getSource(searchPhrase);
				break;
			default:
				translations = reader.getTranslation(searchPhrase);
				break;
		}

		String results = "";
		if (translations.size() != 0)
		{
			statusLabel.setText("Building list…");
			results += "<html>";
			for (String ct: translations)
			{
				results += "<p>";
				results += ct;
				results += "</p>";
			}
			results += "</html>";
			editorPane.setText(results);
			editorPane.setSelectionStart(0);
			editorPane.setSelectionEnd(0);
			statusLabel.setText("Ready");
		} else
			statusLabel.setText("No matches");

	}

	public void setSourceLanguage(String _sourceLanguage)
	{
		reader.setSourceLanguage(_sourceLanguage);
	}

	public void setTargetLanguage(String _targetLanguage)
	{
		reader.setTargetLanguage(_targetLanguage);
	}

	public String getSourceLanguage()
	{
		return reader.getSourceLanguage();
	}

	public String getTargetLanguage()
	{
		return reader.getTargetLanguage();
	}
}
