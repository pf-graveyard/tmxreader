/*

 Copyright (C) 2011 Oleksandr Natalenko aka post-factum

 This program is free software; you can redistribute it and/or modify
 it under the terms of the Universal Program License as published by
 Oleksandr Natalenko aka post-factum; see file COPYING for details.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 You should have received a copy of the Universal Program
 License along with this program; if not, write to
 pfactum@gmail.com

*/

package org.pf.tmxreader;

import java.io.File;
import java.io.FileInputStream;

public class CharsetDetector
{
	private File file;

	public CharsetDetector(File _file)
	{
		file = _file;
	}

	public String getEncoding()
	{
		String encoding = "UTF-8";
		try
		{
			FileInputStream fis = new FileInputStream(file);
			byte[] header = new byte[4];
			fis.read(header);
			fis.close();
			if (header[0] == (byte)0xEF &&
				header[1] == (byte)0xBB &&
				header[2] == (byte)0xBF)
				encoding = "UTF-8";
			else
			if (header[0] == (byte)0xFE &&
				header[1] == (byte)0xFF)
				encoding = "UTF-16";
			else
			if (header[0] == (byte)0xFF &&
				header[1] == (byte)0xFE)
				encoding = "UTF-16";
		} catch (Exception ex)
		{
			System.err.println(ex.getLocalizedMessage());
		}
		return encoding;
	}
}
