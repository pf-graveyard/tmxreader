/*

 Copyright (C) 2011 Oleksandr Natalenko aka post-factum

 This program is free software; you can redistribute it and/or modify
 it under the terms of the Universal Program License as published by
 Oleksandr Natalenko aka post-factum; see file COPYING for details.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 You should have received a copy of the Universal Program
 License along with this program; if not, write to
 pfactum@gmail.com

*/

package org.pf.tmxreader;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.util.ArrayList;

public class TMXReader
{
	private Document doc;
	private String sourceLanguage = "EN-US", targetLanguage = "UK";

	public TMXReader(File _file)
	{
		try
		{
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();

			//disable scheme validating
			builderFactory.setValidating(false);
			builderFactory.setFeature("http://xml.org/sax/features/namespaces", false);
			builderFactory.setFeature("http://xml.org/sax/features/validation", false);
			builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
			builderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			builderFactory.setFeature("http://apache.org/xml/features/continue-after-fatal-error", true);

			DocumentBuilder builder = builderFactory.newDocumentBuilder();

			InputStream inputStream = new FileInputStream(_file);
			CharsetDetector cd = new CharsetDetector(_file);
			Reader reader = new InputStreamReader(inputStream, cd.getEncoding());
			FilterReader fir = new XMLFilterReader(reader);
			InputSource is = new InputSource(fir);

			doc = builder.parse(is);
		} catch (Exception ex)
		{
			System.err.println(ex.getLocalizedMessage());
		}

		//autodetect source language
		NodeList nl = doc.getElementsByTagName("header");
		for (int i = 0; i < nl.getLength(); i++)
		{
			Node cn = nl.item(i);
			if (cn.hasAttributes())
			{
				Node srcLang = cn.getAttributes().getNamedItem("srclang");
				if (srcLang != null)
				{
					sourceLanguage = srcLang.getNodeValue();
					break;
				}
			}
		}

		//autodetect target language
		nl = doc.getElementsByTagName("tuv");
		for (int i = 0; i < nl.getLength(); i++)
		{
			Node cn = nl.item(i);
			if (cn.hasAttributes())
			{
				Node lang = cn.getAttributes().getNamedItem("xml:lang");
				if (lang != null && !lang.getNodeValue().equals(sourceLanguage))
				{
					targetLanguage = lang.getNodeValue();
					break;
				}
			}
		}
	}

	private ArrayList<String> getPairedDomain(String _searchLanguage, String _targetLanguage, String _searchPhrase)
	{
		//NOTE: no expectation this is perfect algorithm despite its speed (fast enough, but consumes memory)
		//If it's possible to optimize it by speed and memory consumption — you're welcome
		ArrayList<String> results = new ArrayList<String>();
		NodeList nl = doc.getElementsByTagName("tu");
		for (int i = 0; i < nl.getLength(); i++)
		{
			NodeList children = nl.item(i).getChildNodes();
			for (int k = 0; k < children.getLength(); k++)
			{
				Node tuvSearch = children.item(k);
				if (tuvSearch.hasAttributes())
				{
					Node langSearch = tuvSearch.getAttributes().getNamedItem("xml:lang");
					if (langSearch != null && langSearch.getNodeValue().equals(_searchLanguage))
					{
						NodeList searchNodes = tuvSearch.getChildNodes();
						for (int j = 0; j < searchNodes.getLength(); j++)
						{
							Node cn = searchNodes.item(j);
							Node firstChild = cn.getFirstChild();
							if (firstChild != null)
								if (firstChild.getNodeValue() != null)
									if (cn.getNodeName().equals("seg") && firstChild.getNodeValue().contains(_searchPhrase))
									{
										for (int n = 0; n < children.getLength(); n++)
										{
											Node tuvTarget = children.item(n);
											if (tuvTarget.hasAttributes())
											{
												Node langTarget = tuvTarget.getAttributes().getNamedItem("xml:lang");
												if (langTarget != null && langTarget.getNodeValue().equals(_targetLanguage))
												{
													NodeList targetNodes = tuvTarget.getChildNodes();
													for (int w = 0; w < searchNodes.getLength(); w++)
													{
														Node ctn = targetNodes.item(w);
														if (ctn.getNodeName().equals("seg"))
														{
															String result = "";
															result += "<font color=\"green\">";

															String subString = "";
															String phrase = cn.getFirstChild().getNodeValue();
															int pos = phrase.indexOf(_searchPhrase);
															//copy string before search phrase
															result += phrase.substring(0, pos);

															//copy search phrase
															subString += "</font>";
															subString += "<font color=\"red\">";
															subString += phrase.substring(pos, pos + _searchPhrase.length());
															subString += "</font>";
															subString += "<font color=\"green\">";
															result += subString;

															//copy string after search phrase
															result += phrase.substring(pos + _searchPhrase.length());
															result += "</font>";
															result += "<br/>";
															result += "<font color=\"blue\">";
															result += ctn.getFirstChild().getNodeValue();
															result += "</font>";

															results.add(result);
														}
													}
												}
											}
										}
									}
						}
					}
				}
			}
		}
		return results;
	}

	public ArrayList<String> getTranslation(String _source)
	{
		return getPairedDomain(sourceLanguage, targetLanguage, _source);
	}

	public ArrayList<String> getSource(String _translation)
	{
		return getPairedDomain(targetLanguage, sourceLanguage, _translation);
	}

	public String getSourceLanguage()
	{
		return sourceLanguage;
	}

	public String getTargetLanguage()
	{
		return targetLanguage;
	}

	public void setSourceLanguage(String _sourceLanguage)
	{
		sourceLanguage = _sourceLanguage;
	}

	public void setTargetLanguage(String _targetLanguage)
	{
		targetLanguage = _targetLanguage;
	}
}
